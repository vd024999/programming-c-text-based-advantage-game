// NPC.h

#ifndef NPC_H
#define NPC_H

#include <string>

class NPC {
public:
    NPC(const std::string& name, const std::string& welcomeMessage);
    std::string GetName() const;
    std::string GetWelcomeMessage() const;
private:
    std::string name_;
    std::string welcomeMessage_;
};

#endif // NPC_H
