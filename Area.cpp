// Area.cpp
#include "Area.h"
#include "NPC.h"

Area::Area() {}

Area::~Area() {
    for (auto& pair : rooms) {
        delete pair.second;
    }
    rooms.clear();
}

void Area::AddRoom(const std::string& name, Room* room) {
    rooms[name] = room;
}

Room* Area::GetRoom(const std::string& name) {
    auto it = rooms.find(name);
    if (it != rooms.end()) {
        return it->second;
    }
    return nullptr;
}

void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
    Room* room1 = GetRoom(room1Name);
    Room* room2 = GetRoom(room2Name);
    if (room1 && room2) {
        room1->AddExitRoom(direction, room2);
        room2->AddExitRoom(GetOppositeDirection(direction), room1);
    } else {
        std::cerr << "One or both rooms not found!" << std::endl;
    }
}

void Area::LoadMapFromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Failed to open file: " << filename << std::endl;
        return;
    }

    std::string line;
    while (std::getline(file, line)) {
        if (line.empty() || line.find("//") == 0) {
            continue; // Skip comments and empty lines
        }
        std::istringstream iss(line);
        std::string command;
        iss >> command;

        if (command == "Room") {
            std::string roomName, description;
            iss >> roomName;
            std::getline(iss, description);
            Room* room = new Room(description);
            AddRoom(roomName, room);
        } else if (command == "Define") {
            std::string roomName1, roomName2, direction;
            iss >> roomName1 >> roomName2 >> direction;
            ConnectRooms(roomName1, roomName2, direction);
        } else if (command == "Create") {
            std::string itemName, itemDesc, roomName;
            iss >> itemName;
            // Read quoted item description
            std::getline(iss >> std::ws, itemDesc, '"');
            std::getline(iss >> std::ws, itemDesc, '"');
            iss >> roomName;
            Room* room = GetRoom(roomName);
            if (room) {
                Item item(itemName, itemDesc);
                room->AddItem(item);
            } else {
                std::cerr << "Room not found for item: " << roomName << std::endl;
            }
        } else if (command == "NPC") {
            std::string npcName, roomName;
            iss >> npcName >> roomName;
            Room* room = GetRoom(roomName);
            if (room) {
                NPC* npc = new NPC(npcName, "Welcome you must solve the puzzle to earn some loot");
                room->AddNPC(npc);
            } else {
                std::cerr << "Room not found for NPC: " << roomName << std::endl;
            }
        }
    }
    file.close();
}

std::string Area::GetOppositeDirection(const std::string& direction) {
    if (direction == "north") return "south";
    if (direction == "south") return "north";
    if (direction == "east") return "west";
    if (direction == "west") return "east";
    // Add more directions if needed
    return "";
}
