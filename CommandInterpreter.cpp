#include "CommandInterpreter.h"
#include "HangmanGame.h"
#include <iostream>
#include <sstream>
#include "Weapon.h"

CommandInterpreter::CommandInterpreter(Player* player, LootBox* lootBox) : player(player), lootBox(lootBox) {}

void CommandInterpreter::InterpretCommand(const std::string& command) {
    std::istringstream iss(command);
    std::string action, argument;
    iss >> action;

    if (action == "move") {
        iss >> argument;
        player->Move(argument);
    } else if (action == "look") {
        player->LookAround();
    } else if (action == "pickup") {
        iss >> argument;
        player->PickUpItem(argument);
    } else if (action == "do") {
        iss >> argument;
        if (argument == "puzzle") {
            if (player->HasItem("Puzzle")) {
                std::cout << " Hangman game will be played here." << std::endl;
                // Pass the player object to the PlayHangman function
                HangmanGame::PlayHangman(*player);
            } else {
                std::cout << "You need the 'Puzzle' item to solve this puzzle." << std::endl;
            }
        } else {
            std::cout << "Unknown action: " << argument << std::endl;
        }
    } else if (action == "spin") {
        if (player->HasItem("LootBox")) {
            if (player->GetTokens() >= 1) {
                player->UseToken(); // Use 1 token
                auto result = lootBox->SpinLootBox();
                std::cout << result << std::endl;
            } else {
                std::cout << "You don't have enough tokens to spin the loot box." << std::endl;
            }
        } else {
            std::cout << "You need the 'LootBox' item to spin a lootbox." << std::endl;
        }
    } else if (action == "view") {
        iss >> argument;
        if (argument == "inv") {
            player->ViewInventory();
        } else if (argument == "health") {
            player->GetHealth();
        } else if (argument == "token") {
            player->ViewTokens();
        } else if (argument == "weapons-list") {
            ViewWeaponList();
        } else {
            std::cout << "Unknown argument for 'view' command: " << argument << std::endl;
        }
    } else if (action == "drop") {
        iss >> argument;
        player->DropItem(argument);
    } else if (action == "talk") {
        player->InteractWithNPC();
    } else if (action == "help") {
        DisplayHelp();
    } else {
        std::cout << "Unknown command: " << action << std::endl;
    }
}





void CommandInterpreter::ViewWeaponList() const {
    std::cout << "Weapon Damage Values:" << std::endl;
    // Define weapon stats
    int swordBaseDamage[] = {60, 130, 290, 500};
    int swordCritDamage[] = {65, 182, 435, 1000};
    int bowBaseDamage[] = {35, 80, 180, 420};
    int bowCritDamage[] = {46, 112, 270, 840};
    int crossbowBaseDamage[] = {50, 115, 225, 470};
    int crossbowCritDamage[] = {65, 161, 338, 940};

    Weapon sword("Sword", swordBaseDamage, swordCritDamage, 0.35);
    Weapon bow("Bow & Arrow", bowBaseDamage, bowCritDamage, 0.05);
    Weapon crossbow("Crossbow", crossbowBaseDamage, crossbowCritDamage, 0.20);

    // Display weapon stats
    std::cout << sword.GetName() << " Base Damage:" << std::endl;
    // Display base damage for each rarity
    std::cout << "  Common: " << sword.GetBaseDamage(Weapon::Rarity::Common) << std::endl;
    std::cout << "  Rare: " << sword.GetBaseDamage(Weapon::Rarity::Rare) << std::endl;
    std::cout << "  Legendary: " << sword.GetBaseDamage(Weapon::Rarity::Legendary) << std::endl;
    std::cout << "  Mythic: " << sword.GetBaseDamage(Weapon::Rarity::Mythic) << std::endl;

    std::cout << sword.GetName() << " Crit Damage:" << std::endl;
    // Display crit damage for each rarity
    std::cout << "  Common: " << sword.GetCritDamage(Weapon::Rarity::Common) << std::endl;
    std::cout << "  Rare: " << sword.GetCritDamage(Weapon::Rarity::Rare) << std::endl;
    std::cout << "  Legendary: " << sword.GetCritDamage(Weapon::Rarity::Legendary) << std::endl;
    std::cout << "  Mythic: " << sword.GetCritDamage(Weapon::Rarity::Mythic) << std::endl;

    // Bow stats
    std::cout << bow.GetName() << " Base Damage:" << std::endl;
    std::cout << "  Common: " << bow.GetBaseDamage(Weapon::Rarity::Common) << std::endl;
    std::cout << "  Rare: " << bow.GetBaseDamage(Weapon::Rarity::Rare) << std::endl;
    std::cout << "  Legendary: " << bow.GetBaseDamage(Weapon::Rarity::Legendary) << std::endl;
    std::cout << "  Mythic: " << bow.GetBaseDamage(Weapon::Rarity::Mythic) << std::endl;

    std::cout << bow.GetName() << " Crit Damage:" << std::endl;
    std::cout << "  Common: " << bow.GetCritDamage(Weapon::Rarity::Common) << std::endl;
    std::cout << "  Rare: " << bow.GetCritDamage(Weapon::Rarity::Rare) << std::endl;
    std::cout << "  Legendary: " << bow.GetCritDamage(Weapon::Rarity::Legendary) << std::endl;
    std::cout << "  Mythic: " << bow.GetCritDamage(Weapon::Rarity::Mythic) << std::endl;

    // Crossbow stats
    std::cout << crossbow.GetName() << " Base Damage:" << std::endl;
    std::cout << "  Common: " << crossbow.GetBaseDamage(Weapon::Rarity::Common) << std::endl;
    std::cout << "  Rare: " << crossbow.GetBaseDamage(Weapon::Rarity::Rare) << std::endl;
    std::cout << "  Legendary: " << crossbow.GetBaseDamage(Weapon::Rarity::Legendary) << std::endl;
    std::cout << "  Mythic: " << crossbow.GetBaseDamage(Weapon::Rarity::Mythic) << std::endl;

    std::cout << crossbow.GetName() << " Crit Damage:" << std::endl;
    std::cout << "  Common: " << crossbow.GetCritDamage(Weapon::Rarity::Common) << std::endl;
    std::cout << "  Rare: " << crossbow.GetCritDamage(Weapon::Rarity::Rare) << std::endl;
    std::cout << "  Legendary: " << crossbow.GetCritDamage(Weapon::Rarity::Legendary) << std::endl;
    std::cout << "  Mythic: " << crossbow.GetCritDamage(Weapon::Rarity::Mythic) << std::endl;
}



void CommandInterpreter::DisplayHelp() const {
    // Display help information for each command
    std::cout << "Available commands:" << std::endl;
    std::cout << "  move [direction] - Move to a different room in the specified direction." << std::endl;
    std::cout << "  look - Look around the current room." << std::endl;
    std::cout << "  pickup [item] - Pick up an item from the current room." << std::endl;
    std::cout << "  do puzzle - Solve a puzzle if you have the required item." << std::endl;
    std::cout << "  spin - Spin the loot box if you have enough tokens." << std::endl;
    std::cout << "  view [inv/health/token/weapons-list] - View inventory, health, tokens, or weapon damage." << std::endl;
    std::cout << "  help - Display available commands and their descriptions." << std::endl;
}
