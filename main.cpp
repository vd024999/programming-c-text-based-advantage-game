#include <iostream>
#include <string>
#include "Area.h"
#include "Player.h"
#include "CommandInterpreter.h"
#include "LootBox.h"

int main() {
    Area gameArea;
    gameArea.LoadMapFromFile("game_map.txt");

    Player player("Alice", 100);

    LootBox lootBox;

    Room* startRoom = gameArea.GetRoom("startRoom");
    if (startRoom) {
        player.SetLocation(startRoom);
        std::cout << startRoom->GetDescription() << std::endl; // Print room description without "You are in: "
    } else {
        std::cerr << "Start room not found!" << std::endl;
        return 1;
    }

    CommandInterpreter commandInterpreter(&player, &lootBox);

    // Game loop
    while (true) {
        std::cout << "Player's Health: " << player.GetHealth() << std::endl;
        std::cout << "Options: ";
        std::cout << "1. Enter command | ";
        std::cout << "2. Quit | ";
        std::cout << "3. Help" << std::endl;

        int choice;
        std::cin >> choice;

        if (choice == 1) {
            std::string command;
            std::cout << "Enter command: ";
            std::cin.ignore(); // Ignore newline character from previous input
            std::getline(std::cin, command);
            commandInterpreter.InterpretCommand(command);
        } else if (choice == 2) {
            std::cout << "Goodbye!" << std::endl;
            break;
        } else if (choice == 3) {
            std::cout << "Available commands:" << std::endl;
            std::cout << "- move [direction]: Move to another room" << std::endl;
            std::cout << "- look: Examine the current room" << std::endl;
            std::cout << "- pickup [item]: Pick up an item from the room" << std::endl;
            std::cout << "- talk: Interact with an NPC in the room" << std::endl;
            std::cout << "- spin: Spin the loot box" << std::endl;
            std::cout << "- view inv: View inventory" << std::endl;
            std::cout << "- view health: View player's health" << std::endl;
            std::cout << "- view token: View player's tokens" << std::endl;
            std::cout << "- view sword-damage: View weapon damage" << std::endl;
            std::cout << "- help: Display available commands" << std::endl;
        } else {
            std::cout << "Invalid choice. Try again." << std::endl;
        }
    }

    return 0;
}
