// Weapon.cpp

#include "Weapon.h"

Weapon::Weapon(std::string name, int baseDamage[], int critDamage[], double critChance)
        : name(name), critChance(critChance) {
    for (int i = 0; i < 4; ++i) {
        this->baseDamage[i] = baseDamage[i];
        this->critDamage[i] = critDamage[i];
    }
}

std::string Weapon::GetName() const {
    return name;
}

int Weapon::GetBaseDamage(Rarity rarity) const {
    return baseDamage[static_cast<int>(rarity)];
}

int Weapon::GetCritDamage(Rarity rarity) const {
    return critDamage[static_cast<int>(rarity)];
}

double Weapon::GetCritChance() const {
    return critChance;
}