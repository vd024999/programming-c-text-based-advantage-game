#include "HangmanGame.h"
#include <iostream>
#include <cstdlib> // For srand() and rand()
#include <ctime>   // For time()

const std::vector<std::string> HangmanGame::words = {
        "abruptly", "absurd", "abyss", "affix", "askew", "avenue", "awkward", "axiom", "azure", "bagpipes",
        "bandwagon", "banjo", "bayou", "beekeeper", "bikini", "blitz", "blizzard", "boggle", "bookworm",
        "boxcar", "boxful", "buckaroo", "buffalo", "buffoon", "buxom", "buzzard", "buzzing", "buzzwords",
        "caliph", "cobweb", "cockiness", "croquet", "crypt", "curacao", "cycle", "daiquiri", "dirndl",
        "disavow", "dizzying", "duplex", "dwarves", "embezzle", "equip", "espionage", "euouae", "exodus",
        "faking", "fishhook", "fixable", "fjord", "flapjack", "flopping", "fluffiness", "flyby", "foxglove",
        "frazzled", "frizzled", "fuchsia", "funny", "gabby", "galaxy", "galvanize", "gazebo", "giaour",
        "gizmo", "glowworm", "glyph", "gnarly", "gnostic", "gossip", "grogginess", "haiku", "haphazard",
        "hyphen", "iatrogenic", "icebox", "injury", "ivory", "ivy", "jackpot", "jaundice", "jawbreaker",
        "jaywalk", "jazziest", "jazzy", "jelly", "jigsaw", "jinx", "jiujitsu", "jockey", "jogging", "joking",
        "jovial", "joyful", "juicy", "jukebox", "jumbo", "kayak", "kazoo", "keyhole", "khaki", "kilobyte",
        "kiosk", "kitsch", "kiwifruit", "klutz", "knapsack", "larynx", "lengths", "lucky", "luxury", "lymph",
        "marquis", "matrix", "megahertz", "microwave", "mnemonic", "mystify", "naphtha", "nightclub", "nowadays",
        "numbskull", "nymph", "onyx", "ovary", "oxidize", "oxygen", "pajama", "peekaboo", "phlegm", "pixel",
        "pizazz", "pneumonia", "polka", "pshaw", "psyche", "puppy", "puzzling", "quartz", "queue", "quips",
        "quixotic", "quiz", "quizzes", "quorum", "razzmatazz", "rhubarb", "rhythm", "rickshaw", "schnapps",
        "scratch", "shiv", "snazzy", "sphinx", "spritz", "squawk", "staff", "strength", "strengths", "stretch",
        "stronghold", "stymied", "subway", "swivel", "syndrome", "thriftless", "thumbscrew", "topaz",
        "transcript", "transgress", "transplant", "triphthong", "twelfth", "twelfths", "unknown", "unworthy",
        "unzip", "uptown", "vaporize", "vixen", "vodka", "voodoo", "vortex", "voyeurism", "walkway", "waltz",
        "wave", "wavy", "waxy", "wellspring", "wheezy", "whiskey", "whizzing", "whomever", "wimpy",
        "witchcraft", "wizard", "woozy", "wristwatch", "wyvern", "xylophone", "yachtsman", "yippee", "yoked",
        "youthful", "yummy", "zephyr", "zigzag", "zigzagging", "zilch", "zipper", "zodiac", "zombie"
};

void HangmanGame::PlayHangman(Player& player) {
    // Set up the word to guess and initialize the game
    std::string wordToGuess = SelectRandomWord();
    std::string guessedWord(wordToGuess.length(), '_'); // Initialize guessed word with underscores
    int attemptsLeft = 6; // Number of attempts allowed
    std::string guessedLetters;

    // Seed the random number generator for generating random words in the future
    srand(static_cast<unsigned int>(time(0)));

    // Main game loop
    while (attemptsLeft > 0) {
        // Display game status
        std::cout << "Word to guess: " << guessedWord << std::endl;
        std::cout << "Attempts left: " << attemptsLeft << std::endl;
        std::cout << "Guessed letters so far: " << guessedLetters << std::endl;

        // Get the player's guess
        char guess;
        std::cout << "Enter a letter: ";
        std::cin >> guess;

        // Check if the guess is correct
        if (wordToGuess.find(guess) != std::string::npos) {
            // Update the guessed word with the correct guess
            for (size_t i = 0; i < wordToGuess.length(); ++i) {
                if (wordToGuess[i] == guess) {
                    guessedWord[i] = guess;
                }
            }
            std::cout << "Correct guess!" << std::endl;
        } else {
            std::cout << "Incorrect guess!" << std::endl;
            --attemptsLeft; // Decrement attempts left
        }

        // Add the guessed letter to the list of guessed letters
        guessedLetters += guess;
        guessedLetters += ' ';

        // Check if the player has guessed the entire word
        if (guessedWord == wordToGuess) {
            std::cout << "Congratulations! You guessed the word: " << wordToGuess << std::endl;
            player.AddToken(); // Add token to inventory
            std::cout << "Token added to inventory!" << std::endl;
            break;
        }
    }

    // If the player runs out of attempts
    if (attemptsLeft == 0) {
        std::cout << "You ran out of attempts. The word was: " << wordToGuess << std::endl;
    }
}

std::string HangmanGame::SelectRandomWord() {
    // Generate a random index within the range of available words
    int index = rand() % words.size();
    // Return the randomly selected word
    return words[index];
}
