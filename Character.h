// Character.h

#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>

class Character {
private:
    std::string name;
    int health;

public:
    Character(const std::string& name, int health);
    void TakeDamage(int damage);
    int GetHealth() const; // Getter method for health
};

#endif // CHARACTER_H