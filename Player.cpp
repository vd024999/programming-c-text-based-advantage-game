// Player.cpp

#include "Player.h"
#include "NPC.h"
#include <iostream>
#include <algorithm>

Player::Player(const std::string& name, int health) : name_(name), health_(health), location_(nullptr), tokens_(0) {}

void Player::SetLocation(Room* room) {
    location_ = room;
}

Room* Player::GetLocation() const {
    return location_;
}

int Player::GetHealth() const {
    return health_;
}

void Player::Move(const std::string& direction) {
    Room* nextRoom = location_->GetExit(direction);
    if (nextRoom != nullptr) {
        SetLocation(nextRoom);
        std::cout << "You move " << direction << " to " << location_->GetDescription() << "." << std::endl;
    } else {
        std::cout << "You can't go " << direction << "." << std::endl;
    }
}

void Player::LookAround() {
    Room* currentRoom = GetLocation();
    if (currentRoom) {
        std::cout << "You look around the room." << std::endl;
        std::cout << currentRoom->GetDescription() << std::endl;

        // Display items in the room
        const std::vector<Item>& items = currentRoom->GetItems();
        if (!items.empty()) {
            std::cout << "You see the following items in the room:" << std::endl;
            for (const auto& item : items) {
                std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
            }
        } else {
            std::cout << "There are no items in the room." << std::endl;
        }

        // Check if an NPC is present in the room
        NPC* npc = currentRoom->GetNPC();
        if (npc) {
            std::cout << "You see someone in the room: " << npc->GetName() << std::endl;
        }
    } else {
        std::cout << "You are not currently in any room." << std::endl;
    }
}


void Player::PickUpItem(const std::string& itemName) {
    const std::vector<Item>& items = location_->GetItems();
    auto it = std::find_if(items.begin(), items.end(), [&itemName](const Item& item) {
        return item.GetName() == itemName;
    });

    if (it != items.end()) {
        std::cout << "You pick up the " << itemName << "." << std::endl;
        inventory_.push_back(*it);
        location_->RemoveItem(*it);
    } else {
        std::cout << "The item " << itemName << " is not in this room." << std::endl;
    }
}

void Player::InteractWithNPC() {
    Room* currentRoom = GetLocation();
    if (currentRoom && currentRoom->GetNPC()) {
        NPC* npc = currentRoom->GetNPC();
        std::cout << npc->GetWelcomeMessage() << std::endl;
        currentRoom->RemoveNPC(); // Remove the NPC after interaction
    } else {
        std::cout << "There is no NPC to interact with." << std::endl;
    }
}

void Player::DropItem(const std::string& itemName) {
    auto it = std::find_if(inventory_.begin(), inventory_.end(), [&itemName](const Item& item) {
        return item.GetName() == itemName;
    });

    if (it != inventory_.end()) {
        std::cout << "You drop the " << itemName << "." << std::endl;
        location_->AddItem(*it);
        inventory_.erase(it);
    } else {
        std::cout << "You don't have the " << itemName << " in your inventory." << std::endl;
    }
}

void Player::ViewInventory() {
    std::cout << "Inventory:" << std::endl;
    if (inventory_.empty()) {
        std::cout << "Empty" << std::endl;
    } else {
        for (const auto& item : inventory_) {
            std::cout << "- " << item.GetName() << std::endl;
        }
    }
}

void Player::AddToken() {
    tokens_++;
}

void Player::UseToken() {
    if (tokens_ > 0) {
        tokens_--;
    }
}

int Player::GetTokens() const {
    return tokens_;
}

bool Player::HasItem(const std::string& itemName) const {
    return std::find_if(inventory_.begin(), inventory_.end(), [&itemName](const Item& item) {
        return item.GetName() == itemName;
    }) != inventory_.end();
}

void Player::ViewTokens() {
    std::cout << "Tokens: " << tokens_ << std::endl;
}

void Player::DoPuzzle() {
    std::cout << "Performing puzzle..." << std::endl;
}


void Player::SpinLootBox(const std::string& requiredItem) {
    if (HasItem(requiredItem)) {
        if (tokens_ > 0) {
            std::cout << "Spinning the loot box..." << std::endl;
            // Perform loot box spinning logic here
            tokens_--;
        } else {
            std::cout << "You don't have enough tokens to spin the loot box." << std::endl;
        }
    } else {
        std::cout << "You need the " << requiredItem << " to spin the loot box." << std::endl;
    }
}