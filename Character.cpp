// Character.cpp

#include "Character.h"

Character::Character(const std::string& name, int health) : name(name), health(health) {}

void Character::TakeDamage(int damage) {
    health -= damage;
}

int Character::GetHealth() const {
    return health;
}