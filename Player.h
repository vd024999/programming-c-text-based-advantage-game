// Player.h

#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <vector>
#include "Room.h"
#include "Item.h"

class Player {
public:
    Player(const std::string& name, int health);
    void SetLocation(Room* room);
    Room* GetLocation() const;
    int GetHealth() const;
    void Move(const std::string& direction);
    void LookAround();
    void PickUpItem(const std::string& itemName);
    void InteractWithNPC();
    void DropItem(const std::string& itemName);
    void ViewInventory();
    void AddToken();
    void UseToken();
    int GetTokens() const;
    bool HasItem(const std::string& itemName) const;
    void ViewTokens();
    void DoPuzzle();
    void SpinLootBox(const std::string& requiredItem); // Declaration added
private:
    std::string name_;
    int health_;
    Room* location_;
    std::vector<Item> inventory_;
    int tokens_;
};

#endif // PLAYER_H
