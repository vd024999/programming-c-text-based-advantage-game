// Inventory.h
#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>
#include "GameObject.h"

class Inventory {
private:
    std::vector<GameObject*> items; // Use pointers for polymorphic behavior

public:
    ~Inventory(); // Destructor to clean up dynamically allocated memory
    void AddItem(GameObject* item); // Method to add an item to the inventory
    void UseItem(GameObject* item, GameObject& target); // Method to use an item on a target
};

#endif // INVENTORY_H