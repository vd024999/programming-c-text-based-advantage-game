// Room.h

#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <vector>
#include <map>
#include "Item.h"

class NPC; // Forward declaration

class Room {
public:
    Room(const std::string& description);
    void SetExit(const std::string& direction, Room* neighbor);
    Room* GetExit(const std::string& direction) const;
    std::string GetDescription() const;
    void AddItem(const Item& item);
    void RemoveItem(const Item& item);
    const std::vector<Item>& GetItems() const;
    void AddNPC(NPC* npc); // Function to add an NPC
    void RemoveNPC(); // Function to remove the NPC
    NPC* GetNPC() const;
    void AddExitRoom(const std::string& direction, Room* room);


private:
    std::string description_;
    std::vector<Item> items_;
    std::map<std::string, Room*> exits_;
    NPC* npc_; // Pointer to NPC
};

#endif // ROOM_H
