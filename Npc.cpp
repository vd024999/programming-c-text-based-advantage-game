// NPC.cpp

#include "NPC.h"

NPC::NPC(const std::string& name, const std::string& welcomeMessage) : name_(name), welcomeMessage_(welcomeMessage) {}

std::string NPC::GetName() const {
    return name_;
}

std::string NPC::GetWelcomeMessage() const {
    return welcomeMessage_;
}
