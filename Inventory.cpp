// Inventory.cpp
#include "Inventory.h"

Inventory::~Inventory() {
    // Clean up dynamically allocated memory
    for (auto item : items) {
        delete item;
    }
    items.clear();
}

void Inventory::AddItem(GameObject* item) {
    // Implement logic to add item to inventory
    items.push_back(item);
}

void Inventory::UseItem(GameObject* item, GameObject& target) {
    // Implement logic to use an item on a target object
    item->Interact(); // Example of interaction, you can extend this method based on your game logic
}