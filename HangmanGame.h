#ifndef HANGMANGAME_H
#define HANGMANGAME_H

#include <string>
#include <vector>
#include "Player.h" // Include Player header for passing player object

class HangmanGame {
public:
    static void PlayHangman(Player& player); // Static method to play the hangman game

private:
    static std::string SelectRandomWord(); // Method to select a random word from the list
    static const std::vector<std::string> words; // Static list of words for the hangman game
};

#endif // HANGMANGAME_H
