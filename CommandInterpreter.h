// CommandInterpreter.h

#ifndef COMMANDINTERPRETER_H
#define COMMANDINTERPRETER_H

#include <string>
#include "Player.h"
#include "LootBox.h" // Include LootBox header file

class CommandInterpreter {
public:
    CommandInterpreter(Player* player, LootBox* lootBox); // Constructor
    void InterpretCommand(const std::string& command); // Interpret user command

private:
    Player* player;
    LootBox* lootBox; // Pointer to LootBox object

    void ViewWeaponList() const; // Function to view weapon list
    void DisplayHelp() const; // Function to display help information
};

#endif // COMMANDINTERPRETER_H
