// Room.cpp

#include "Room.h"
#include "NPC.h"
#include <iostream>
#include <algorithm>


Room::Room(const std::string& description) : description_(description), npc_(nullptr) {}

void Room::SetExit(const std::string& direction, Room* neighbor) {
    exits_[direction] = neighbor;
}

Room* Room::GetExit(const std::string& direction) const {
    auto it = exits_.find(direction);
    if (it != exits_.end()) {
        return it->second;
    } else {
        return nullptr;
    }
}

std::string Room::GetDescription() const {
    return description_;
}

void Room::AddItem(const Item& item) {
    items_.push_back(item);
}

void Room::RemoveItem(const Item& item) {
    auto it = std::find(items_.begin(), items_.end(), item);
    if (it != items_.end()) {
        items_.erase(it);
    }
}

const std::vector<Item>& Room::GetItems() const {
    return items_;
}

void Room::AddNPC(NPC* npc) {
    npc_ = npc;
}

void Room::RemoveNPC() {
    npc_ = nullptr;
}


NPC* Room::GetNPC() const {
    return npc_; // Replace npcPtr with your actual member variable storing the NPC pointer
}


void Room::AddExitRoom(const std::string& direction, Room* room) {
    exits_[direction] = room;
}