#ifndef ITEM_H
#define ITEM_H

#include <string>

class Item {
private:
    std::string name;
    std::string description;

public:
    Item(const std::string& name, const std::string& desc);
    void Interact() const;
    const std::string& GetName() const;
    const std::string& GetDescription() const;

    // Equality operator declaration
    bool operator==(const Item& other) const;
};

#endif // ITEM_H