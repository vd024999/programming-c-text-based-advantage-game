#include "LootBox.h"

std::string LootBox::SpinLootBox() {
    // Randomly select the rarity and weapon type
    std::mt19937 mt(static_cast<unsigned int>(time(nullptr))); // Seed the random number generator
    std::uniform_real_distribution<double> dist(0.0, 1.0);

    double rarityChance = dist(mt);
    double weaponChance = dist(mt);

    std::string rarity;
    if (rarityChance < 0.01) { // Mythic rarity with 0.01% chance
        rarity = "Mythic";
    } else if (rarityChance < 0.1) { // Legendary rarity with 0.1% chance
        rarity = "Legendary";
    } else if (rarityChance < 0.25) { // Rare rarity with 15% chance
        rarity = "Rare";
    } else { // Common rarity with 80% chance
        rarity = "Common";
    }

    std::string weaponType;
    if (weaponChance < 0.25) { // 25% chance of getting each weapon type
        weaponType = "Sword";
    } else if (weaponChance < 0.5) {
        weaponType = "Bow & Arrow";
    } else if (weaponChance < 0.75) {
        weaponType = "Crossbow";
    } else {
        weaponType = "Gun";
    }

    return "You spun the loot box and pulled a " + rarity + " " + weaponType;
}
