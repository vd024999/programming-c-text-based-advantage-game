#include "Item.h"

Item::Item(const std::string& name, const std::string& desc) : name(name), description(desc) {}

void Item::Interact() const {
    // Interaction logic
}

const std::string& Item::GetName() const {
    return name;
}

const std::string& Item::GetDescription() const {
    return description;
}

// Equality operator definition
bool Item::operator==(const Item& other) const {
    return name == other.name && description == other.description;
}