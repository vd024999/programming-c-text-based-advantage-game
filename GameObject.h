// GameObject.h
#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

class GameObject {
public:
    virtual ~GameObject() {}
    virtual void Interact() const = 0; // Define a virtual Interact method for interaction
};

#endif // GAMEOBJECT_H