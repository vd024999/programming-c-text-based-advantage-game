#ifndef LOOTBOX_H
#define LOOTBOX_H

#include <iostream>
#include <string>
#include <random>
#include <ctime>

class LootBox {
public:
    std::string SpinLootBox(); // Method to spin the loot box
};

#endif // LOOTBOX_H
