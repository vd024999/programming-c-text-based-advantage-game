// Weapon.h

#ifndef WEAPON_H
#define WEAPON_H

#include <string>

class Weapon {
public:
    enum class Rarity { Common, Rare, Legendary, Mythic };

    // Constructor
    Weapon(std::string name, int baseDamage[], int critDamage[], double critChance);

    // Getter methods
    std::string GetName() const;
    int GetBaseDamage(Rarity rarity) const;
    int GetCritDamage(Rarity rarity) const;
    double GetCritChance() const;

private:
    std::string name;
    int baseDamage[4];
    int critDamage[4];
    double critChance;
};

#endif // WEAPON_H